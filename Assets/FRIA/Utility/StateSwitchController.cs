﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class StateSwitchController<T>
{
    public T currentState
    {
        get 
        {
            return _current;
        }
    }
    public bool EnableDefinedTransitionOnly
    {
        get;
        private set;
    }

    private T _current;

    private Dictionary<int, SwitchBoard> switchBoardDictionary = new Dictionary<int, SwitchBoard>();
    public event System.Action<T, T> onStateSwitch_ToFrom;

    public StateSwitchController()
    {
        _current = default(T);
    }
    public void Init(bool enableDefinedTransitionOnly, bool loadEmpties = false)
    {
        this.EnableDefinedTransitionOnly = enableDefinedTransitionOnly;
        if (loadEmpties)
        {
            if (typeof(T).IsEnum)
            {
                foreach (var item in System.Enum.GetValues(typeof(T)))
                {
                    AddEmptyState((int)Convert.ChangeType(item, typeof(int)));
                }

            }
        }
    }
    public void AddEmptyState(int state)
    {
        SwitchBoard sb = new SwitchBoard();
        switchBoardDictionary.Add(state, sb);
    }
    public void AddState(T state, List<GameObject> activeList=null)
    {
        int si = (int)Convert.ChangeType(state, typeof(int));

        if (!switchBoardDictionary.ContainsKey(si))
        {
            SwitchBoard sb = new SwitchBoard();
            if (activeList != null)  sb.activeList.AddRange(activeList);
            switchBoardDictionary.Add(si, sb);
        }
        else
        {
            SwitchBoard sb = switchBoardDictionary[si];
            if (activeList != null) sb.activeList.AddRange(activeList);
        }

    }
    public void AddStateEntryCallback(T toState, System.Action onEntry)
    {
        int si = (int)Convert.ChangeType(toState, typeof(int));
        if (!switchBoardDictionary.ContainsKey(si))
        {
            Debug.LogErrorFormat("{0} state dont exist", toState);
        }
        else
        {
            SwitchBoard sb = switchBoardDictionary[si];
            sb.onEntry += onEntry;
        }
    }
    public void AddStateExitCallback(T fromState, System.Action onExit)
    {
        int si = (int)Convert.ChangeType(fromState, typeof(int));
        if (!switchBoardDictionary.ContainsKey(si))
        {
            Debug.LogErrorFormat("{0} state dont exist", fromState);
        }
        else
        {
            SwitchBoard sb = switchBoardDictionary[si];
            sb.onExit += onExit;
        }
    }
    public void AddTransition(T state, T toState, System.Action onTransition=null)
    {
        int si = (int)Convert.ChangeType(state, typeof(int));
        if (!switchBoardDictionary.ContainsKey(si))
        {
            Debug.LogErrorFormat("{0} state dont exist", state);
        }
        else
        {
            SwitchBoard sb = switchBoardDictionary[si];
            if (!sb.transitions.ContainsKey(toState))
            {
                TransitionData td = new TransitionData();
                td.fromState = state;
                td.toState = toState;
                td.onTransition += onTransition;
                sb.transitions.Add(toState, td);
            }
            else
            {
                Debug.LogErrorFormat("Transition Already Exists {0}=>{1}",state,toState);
            }
        }

    }
    public void AddTranstionCallback(T state, T toState, System.Action onTransition)
    {
        int si = (int)Convert.ChangeType(state, typeof(int));
        if (!switchBoardDictionary.ContainsKey(si))
        {
            Debug.LogErrorFormat("{0} state dont exist", state);
        }
        else
        {
            SwitchBoard sb = switchBoardDictionary[si];
            if (!sb.transitions.ContainsKey(toState))
            {
                Debug.LogErrorFormat("Transition doesnt Exists {0}=>{1}", state, toState);
            }
            else
            {
                TransitionData td = sb.transitions[toState];
                td.onTransition += onTransition;
            }
        }
    }

    public void SwitchState(T toState)
    {
        TransitionData td=null;
        int ci = (int)Convert.ChangeType(currentState, typeof(int));
        SwitchBoard oldSwitchBoard = switchBoardDictionary[ci];

        if (EnableDefinedTransitionOnly)
        {
            if (!oldSwitchBoard.transitions.ContainsKey(toState))
            {
                Debug.LogFormat("Transition Denied! {0}=>{1}",currentState,toState);
                return;
            }
        }
        if (oldSwitchBoard.transitions.ContainsKey(toState))
        {
            td = oldSwitchBoard.transitions[toState];
        }

        T prev = _current;
        _current = toState;

        int to_index = (int) Convert.ChangeType(toState, typeof(int));



        foreach (KeyValuePair<int,SwitchBoard> kvp in switchBoardDictionary)
        {
            if ( kvp.Key != to_index)
            {
                foreach (var gameobject in kvp.Value.activeList)
                {
                    gameobject.SetActive(false);
                }
            }
        }
        foreach (var item in switchBoardDictionary[to_index].activeList)
        {
            item.SetActive(true);
        }
        oldSwitchBoard.onExit?.Invoke();
        if (td != null)
        {
            td.onTransition?.Invoke();
        }
        onStateSwitch_ToFrom?.Invoke(prev,toState);
        SwitchBoard newSwitchboard = switchBoardDictionary[to_index];
        newSwitchboard.onEntry?.Invoke();
    }
    private class SwitchBoard
    {
        public List<GameObject> activeList = new List<GameObject>();
        public Dictionary<T,TransitionData> transitions = new Dictionary<T,TransitionData>();
        public System.Action onEntry;
        public System.Action onExit;
    }
    public class TransitionData
    {
        public T fromState;
        public T toState;
        public System.Action onTransition;
    }
}