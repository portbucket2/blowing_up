﻿using System.Collections.Generic;
using UnityEngine;
using LionStudios;
public static class AnalyticsAssistant 
{
    
#if UNITY_EDITOR
    static bool logToConsole = true;
#else
    static bool logToConsole = false;
#endif
    static void Console(string format, params object[] args)
    {
        if (logToConsole) Debug.LogFormat(format, args);
    }

    public static void LevelStarted(int levelNo)
    {
        try
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("level", levelNo);
            Analytics.Events.LevelStarted(dic);
            Console("Started Level {0}", levelNo);
        }
        catch(System.Exception e)
        {
            Debug.LogError(e.Message);
        }
    }
    public static void LevelCompleted(int levelNo)
    {
        try
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("level", levelNo);
            Analytics.Events.LevelComplete(dic);
            Console("Completed Level {0}", levelNo);
        }
        catch (System.Exception e)
        {
            Debug.LogError(e.Message);
        }
    }
    public static void LevelFailed(int levelNo)
    {
        try
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("level", levelNo);
            Analytics.Events.LevelFailed(dic);
            Console("Failed Level {0}", levelNo);
        }
        catch (System.Exception e)
        {
            Debug.LogError(e.Message);
        }
    }


}

