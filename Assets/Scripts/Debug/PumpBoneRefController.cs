﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SkinnedMeshRenderer))]
public class PumpBoneRefController : MonoBehaviour
{
    SkinnedMeshRenderer skin;
    public List<PumpBoneRef> refs;
    // Start is called before the first frame update
    void Awake()
    {
        skin = GetComponent<SkinnedMeshRenderer>();
        foreach (var pumpRef in refs)
        {
            pumpRef.Init(skin);

        }
        skinnedMesh = new Mesh();
    }

    public Mesh skinnedMesh;
    public Vector3[] vs;
    // Update is called once per frame
    void LateUpdate()
    {
         //skin.BakeMesh(skinnedMesh);
        //vs = skinnedMesh.vertices;
        foreach (var pumpRef in refs)
        {
            pumpRef.ReCalc();
        }
    }
    private void OnDrawGizmos()
    {
        foreach (var pumpRef in refs)
        {
            pumpRef.OnDrawGizmos();
        }
    }

}
[System.Serializable]
public class PumpBoneRef
{
    public bool enable;
    public string name;//materialReferencename
    public Transform boneTrans;
    public Vector3 offset;
    public float relativeWeight = 1;
    public Color gizmosCol = Color.yellow;
    public int i1;
    public int i2;

    private SkinnedMeshRenderer skin;
    private Transform skinTrans;

    public void Init(SkinnedMeshRenderer skin)
    {
        this.skin = skin;
        this.skinTrans = skin.transform;
    }
    Vector3 v;
    Vector3 vc;
    public void ReCalc()// Vector3[] vs)
    {
        if (!enable) return;
        v = boneTrans.position + offset.x * boneTrans.right + offset.y * boneTrans.up + offset.z * boneTrans.forward;
        vc = skinTrans.InverseTransformPoint( v);
        //Vector3 v1 = vs[i1];
        //Vector3 v2 = vs[i2];
        //vc = offset + (v1 + v2) / 2;
        skin.material.SetVector(name, new Vector4(vc.x, vc.y, vc.z, relativeWeight));
    }
    public void OnDrawGizmos()
    {
        Gizmos.color = gizmosCol;
        Gizmos.DrawWireSphere(vc, relativeWeight / 2);
    }
}