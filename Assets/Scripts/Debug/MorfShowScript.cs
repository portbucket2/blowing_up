﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MorfShowScript : MonoBehaviour
{
    public List<SkinnedMeshRenderer> rends;
    public Vector2 valueRange;
    // Start is called before the first frame update
    void Start()
    {
        foreach (SkinnedMeshRenderer item in rends)
        {
            item.material = Instantiate(item.material);
        }
        value = valueRange.x;
        speed = (valueRange.y - valueRange.x )*2/ loopTime;
    }

    public float value;
    float speed = 1;
    public float loopTime = 2;
    // Update is called once per frame
    void Update()
    {
        value += speed * Time.deltaTime;
        if (speed > 0)
        {
            if (value >= valueRange.y)
            {
                value = valueRange.y;
                speed = -speed;
            }
        }
        else
        {
            if (value <= valueRange.x)
            {
                value = valueRange.x;
                speed = -speed;
            }
        }

        foreach (var item in rends)
        {
            item.material.SetFloat("_RefRadius", value);
        }
    }
}
