﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuyCanvasController : MonoBehaviour
{
    public Canvas canvasRef;
    public BarUI gasBar;
    public GasController gasCon;
    public AgentStateController stateCon;

    public float offsetDistance = 3;

    Camera cam;

    Transform camTrans;
    Transform rootTrans;
    Transform canvTrans;
    // Start is called before the first frame update
    bool canvasDisabled = false;
    void Start()
    {
        cam = Camera.main;
        canvasRef.worldCamera = cam;

        gasCon.onGasChanged_current_total += OnGasChanged;

        camTrans = cam.transform;
        rootTrans = this.transform;
        canvTrans = canvasRef.transform;
        OnGasChanged(0,gasCon.gasMax);
        stateCon.onTouchDown_xBonus += (int x)=>
        {
            canvasDisabled =  true;
            gasBar.StopAllCoroutines();

            rootTrans.gameObject.SetActive(!canvasDisabled);
        };
    }
    void OnGasChanged(float current, float total)
    {
        if(!canvasDisabled) gasBar.LoadValue(current,total,false,true);
    }

    private void LateUpdate()
    {
        if(!canvasDisabled)
        {
            rootTrans.rotation = camTrans.rotation;
            Vector3 dir = (camTrans.position - rootTrans.position).normalized;
            canvTrans.position = rootTrans.position + dir * offsetDistance;
        }
    }
}
