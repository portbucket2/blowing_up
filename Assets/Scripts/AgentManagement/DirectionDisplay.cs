﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionDisplay : MonoBehaviour
{
    public LayerMask rayLayer;
    public GameObject directionVisualizer;

    GasController gasCon;
    AgentStateController stateCon;
    // Start is called before the first frame update
    float xScale;
    Transform targetTrans;
    void Start()
    {
        targetTrans = directionVisualizer.transform;
        gasCon = GetComponent<GasController>();
        stateCon = GetComponent<AgentStateController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (stateCon.CurrentState == GuyState.FLY)
        {
            RaycastHit rch;
            if (Physics.Raycast(targetTrans.position, gasCon.PushDirection, out rch, 100, rayLayer))
            {
                float dist = (rch.point - targetTrans.position).magnitude;
                //Vector3 midPoint = (rch.point+this.transform.position)/ 2;
                //directionVisualizer.transform.localPosition = new Vector3(0,-dist/2,0);
                Vector3 localScale = new Vector3(targetTrans.localScale.x, dist * 2, targetTrans.localScale.z);
                if(dist<3)
                {
                    directionVisualizer.SetActive(false);
                }
                else
                {
                    directionVisualizer.SetActive(true);
                    targetTrans.localScale = localScale;
                }
                //directionVisualizer.transform.rotation = Quaternion.Euler(0,0,Vector3.Angle(gasCon.PushDirection,Vector3.right));
            }
            else
            {
                Vector3 localScale = new Vector3(targetTrans.localScale.x, 50 * 2, targetTrans.localScale.z);

                directionVisualizer.SetActive(true);
                targetTrans.localScale = localScale;

            }
        }
        else
        {
            directionVisualizer.SetActive(false);
        }

    }
}
