﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class GasController : MonoBehaviour
{
    public bool log;
    AgentStateController stateCon;
    GuyState State { get { return stateCon.CurrentState; } }

    public event System.Action<float, float> onGasChanged_current_total;
    public event System.Action<bool> onValveStateChanged;

    public SkinController skinController;
    public Transform dischargeDirection;
    public GasPlaySettings gasSettings;
    public Animator pumperAnim;
    public ParticleSystem dischargeParticle;
    float gasLeft = 100;
    public float gasMax = 100;

    [HideInInspector] public bool isValveOpen;
    Rigidbody rgbd;

    SphereCollider sphCol;
    bool flightEnabled;
    float flightStartTime;


    public float GasRatio
    {
        get
        {
            return gasLeft / gasMax;
        }
    }
    public Vector3 PushDirection
    {
        get { return -dischargeDirection.up; }
    }
    public float AngleWithForward
    {
        get
        {
            return Vector3.Angle(PushDirection, GameplayManager.Instance.raceDirection);
        }
    }
    public bool AngleWithinLimit(float limit)
    {
            return AngleWithForward <limit;
    }
    private void Start()
    {
        flightEnabled = false;
        stateCon = GetComponent<AgentStateController>();
        stateCon.onPumpStart += GasPumpStart;
        stateCon.onFlightStart += OnFlightStart;
        //stateCon.onFinishLineCross += (AgentStateController agc)=>{ rgbd.drag *=1.5f;};

        skinController.Init();
        rgbd = GetComponent<Rigidbody>();
        sphCol = GetComponentInChildren<SphereCollider>();
        if (rgbd)
        {
            rgbd.drag = gasSettings.drag;
        }

        SetGasAmount(0);
        pumperAnim.transform.parent = null;

    }
    void SetGasAmount(float newGasAmount)
    {
        newGasAmount = Mathf.Clamp(newGasAmount, 0, gasMax);

        gasLeft = newGasAmount;

        float r2 = Mathf.Sqrt(GasRatio);
        if (skinController) skinController.SetRadius(r2);
        if (sphCol) sphCol.radius = r2;
        if (rgbd) rgbd.mass = gasSettings.MassFromGas(gasLeft);
        onGasChanged_current_total?.Invoke(gasLeft, gasMax);
    }

    void GasPumpStart()
    {
        flightEnabled = false;
        pumpRoutine = StartCoroutine(PumpRoutine());
    }
    public void InjectNow()
    {
        pumpAmount += Random.Range(gasSettings.injectPerTap_min, gasSettings.injectPerTap_max);
        lastPumpTime = Time.time;
        if(GasRatio<0.99f)pumperAnim.SetTrigger("pump");
    }
    float pumpAmount;
    float lastPumpTime;
    Coroutine pumpRoutine;
    public IEnumerator PumpRoutine()
    {
        while (true)
        {
            if (pumpAmount > 0)
            {
                float timeLeft = Mathf.Clamp(gasSettings.injectTime - (Time.time - lastPumpTime), 0, gasSettings.injectTime);
                float amountPerTime = pumpAmount / timeLeft;
                float gasFillInThisCycle = amountPerTime * Time.fixedDeltaTime;
                pumpAmount = pumpAmount - gasFillInThisCycle;
                SetGasAmount(gasLeft + gasFillInThisCycle);
            }
            else if(GasRatio<0.9999f)
            {
                SetGasAmount(gasLeft - gasSettings.gasLossPerSecondOnPump*Time.fixedDeltaTime);
            }
            yield return new WaitForFixedUpdate();
        }
    }
    void OnFlightStart()
    {
        if (pumpRoutine != null) StopCoroutine(pumpRoutine);
        flightEnabled = true;
        flightStartTime = Time.time;
        rgbd.AddForce(Vector3.right * gasSettings.initialBoostImpulseForce, ForceMode.Impulse);

    }

    bool ShouldEmitParticle
    {
        get
        {
            switch (stateCon.CurrentState)
            {
                
                case GuyState.FLY:
                case GuyState.LAND:
                    return isValveOpen;
                default:
                    return false;
            }
        }
    }

    float forceReductionTime = 0;
    void FixedUpdate()
    {

        if (flightEnabled)
        {
            if (isValveOpen || stateCon.CurrentState == GuyState.LAND)
            {
                Discharge();
            }
            
            if (!isValveOpen || gasLeft <= 0)
            {
                rgbd.AddForce(gasSettings.GetGravity(GasRatio) * Time.fixedDeltaTime, ForceMode.Acceleration);
            }
        }
        if (ShouldEmitParticle && !dischargeParticle.isEmitting)
            dischargeParticle.Play();
        if (!ShouldEmitParticle && dischargeParticle.isEmitting)
            dischargeParticle.Stop();
    }
    void CheckForProp(GameObject go)
    {            
        if(stateCon.CurrentState == GuyState.FLY)
        {
            BaseGasProp prop = go.GetComponent<BaseGasProp>();
            if(prop && prop.alive)
            {
                prop.Pop(this);
                //Vector3 dir = (this.transform.position-prop.transform.position).normalized;
                //StartCoroutine(GasChange(prop.gasChange,prop.chargeTime,dir,prop.GasForceRatio));
            }

        }

    }
    private void OnCollisionEnter(Collision other) {
        if(other.collider.attachedRigidbody)
                CheckForProp(other.collider.attachedRigidbody.gameObject);
    }


    private void OnTriggerEnter(Collider other) {
               
        if(other.attachedRigidbody)
        {

            CheckForProp(other.attachedRigidbody.gameObject);
            SpringBoardController spring = other.attachedRigidbody.gameObject.GetComponent<SpringBoardController>();
            if(spring)
            {
                if(!spring.triggered) spring.Trigger();
                rgbd.AddForce(spring.forceDirection.up.normalized*spring.forceValue,ForceMode.Impulse);
            }
        }
        //if(other.gameObject.layer == LayerMask.NameToLayer("Props") )Debug.LogFormat("prop {0}, {1} ",prop, prop.triggered);
    }
   /* private void OnCollisionStay(Collision other) 
    {
        SpringBoardController prop = other.gameObject.GetComponent<SpringBoardController>();
        if(prop)
        {
            if(!prop.triggered) prop.Trigger();
        }
        if(other.gameObject.layer == LayerMask.NameToLayer("Props") )Debug.LogFormat("prop {0}, {1} ",prop, prop.triggered);
    }*/
    internal IEnumerator GasChange(float amount, float timeBudget, Vector3 pushDir, float forceRatio)
    {
        float loss;
        float changeAmount;
        float amountMagLeft = Mathf.Abs( amount);
        int dir = (amount>0)?1:-1;
        float rate = amountMagLeft/timeBudget;
        while(amountMagLeft>0)
        {
            float fTime = Time.fixedDeltaTime;
            changeAmount = Mathf.Clamp(fTime*rate,0,amountMagLeft);
            amountMagLeft -= changeAmount;   
            //SetGasAmount(gasLeft + changeAmount*dir);
            loss = -changeAmount*dir;
            GasChangeMidFlight(loss,pushDir, forceRatio);
            yield return new WaitForFixedUpdate();
        }

    }
    void Discharge()
    {       
         if(stateCon.crossedFinishLine) 
        {
            forceReductionTime += 0.25f* Time.fixedDeltaTime;
        }
        float gasLoss = gasSettings.gasLossRate * Time.fixedDeltaTime;
        gasLoss = Mathf.Clamp(gasLoss, 0, gasLeft);
        GasChangeMidFlight(gasLoss,PushDirection);
    }
    void GasChangeMidFlight(float gasLoss,Vector3 pushDir, float forceRatio =1)
    {
        SetGasAmount(gasLeft - gasLoss);
        Vector3 force = pushDir * gasSettings.gasToForceRatio * gasLoss*forceRatio * (1/ (1+forceReductionTime));
        rgbd.AddForce(force, gasSettings.forceMode);
    }

    public void SetValve(bool isOpen)
    {
        if (!flightEnabled) return;
        if (isValveOpen == isOpen) return;
        isValveOpen = isOpen;
        onValveStateChanged?.Invoke(isOpen);
        
    }

}