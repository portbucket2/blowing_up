﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwingController : MonoBehaviour
{
    public AgentStateController stateCon;
    GuyState State { get { return stateCon.CurrentState; } }

    public GasController gasCon;
    GasPlaySettings GasSettings { get { return gasCon.gasSettings; } }

    public Vector3 baseEulerRot = new Vector3(0,0,0);
    public float swingStartLerping =2;
    public float swingEndLerping = 1;

    float swingTime;
    Quaternion baseRotation;
    float flightStartTime;
    float flightEndTime;


    float FlightLerpValue1 { get { return Mathf.Clamp01((Time.time - flightStartTime) / swingStartLerping); } }
    float FlightLerpValue2 { get { return Mathf.Clamp01((Time.time - flightEndTime) / swingEndLerping); } }
    void Start()
    {
        stateCon.onFlightStart += () =>
        {
            flightStartTime = Time.time;
            swingTime = 0;
        };
        stateCon.onLandStart += () =>
        {
            flightEndTime = Time.time;
        };
        baseRotation = Quaternion.Euler(baseEulerRot);
        transform.localRotation = baseRotation;
    }

    void FixedUpdate()
    {
        switch (State)
        {

            case GuyState.FLY:
                IdleSwing();
                transform.localRotation = Quaternion.Slerp(baseRotation, swingTargetRotation, FlightLerpValue1);
                break;
            case GuyState.LAND:
            case GuyState.FAIL:
            case GuyState.FINISH:
                IdleSwing();
                transform.localRotation = Quaternion.Slerp(swingTargetRotation, baseRotation, FlightLerpValue2);
                break;
            default:
                break;
        }

    }

    Quaternion swingTargetRotation;
    void IdleSwing()
    {
        swingTime += Time.fixedDeltaTime * (gasCon.isValveOpen ? 0.32f : 1);
        swingTargetRotation = Quaternion.Euler(0, baseEulerRot.y, (-90 + Mathf.Sin(swingTime * GasSettings.frequency) * GasSettings.maxSwing));
    }
}
