﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class SkinController : MonoBehaviour
{
    public SkinnedMeshRenderer currentSkin;

    public List<SkinnedMeshRenderer> smrList;
    public int currentIndex;

    private List<Material> mats;

    public float currentRadius = 0;
    // Start is called before the first frame update
    public void Init()
    {
        mats = new List<Material>();
        foreach (var skinRenderer in smrList)
        {
            Material skinMat = Instantiate(skinRenderer.material);
            skinRenderer.material = skinMat;
            mats.Add(skinMat);
        }

        ApplySkin();
    }
    public void SetRadius(float rad)
    {
        this.currentRadius = rad;
        if(mats!=null && mats.Count>currentIndex)
            mats[currentIndex].SetFloat("_RefRadius", currentRadius);
    }
    public void ApplySkin()
    {
        for (int i = 0; i < smrList.Count; i++)
        {
            smrList[i].gameObject.SetActive(currentIndex==i);
        }
        currentSkin = smrList[currentIndex];
        SetRadius(currentRadius);
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(SkinController))]
public class SkinController_Editor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        SkinController sc = (SkinController)target;

        if (GUILayout.Button("Apply Current Index"))
        {
            sc.ApplySkin();
        }
    }
}
#endif