﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentStateController : MonoBehaviour
{
    public bool log;
    public GuyState CurrentState
    {
        get { return stateMan.currentState; }
    }
    StateSwitchController<GuyState> stateMan;

    public event System.Action onPumpStart;
    public event System.Action onFlightStart;
    public event System.Action onLandStart;
    public event System.Action<AgentStateController> onFinishLineCross;
    public event System.Action<AgentStateController> onFail;
    public event System.Action<int> onTouchDown_xBonus;
    public Animator anim;
    public Animator punchAnim;
    public LayerMask finishLayerMask;
    // Start is called before the first frame update
    void Awake()
    {
        stateMan = new StateSwitchController<GuyState>();
        stateMan.Init(enableDefinedTransitionOnly: true, loadEmpties: true);
        stateMan.AddTransition(GuyState.INIT, GuyState.PUMP, () => 
        {
            onPumpStart?.Invoke();
            FRIA.Centralizer.Add_DelayedAct(()=> 
            {
                punchAnim.SetTrigger("punch");
            },GameplayManager.Instance.countDownTime);
        });
        stateMan.AddTransition(GuyState.PUMP, GuyState.FLY, () => { onFlightStart?.Invoke(); });
        stateMan.AddTransition(GuyState.FLY, GuyState.LAND, () => { onLandStart?.Invoke(); });
        stateMan.AddTransition(GuyState.LAND, GuyState.FAIL, () => { onFail?.Invoke(this); });
        stateMan.AddTransition(GuyState.LAND, GuyState.FINISH, () => { onTouchDown_xBonus?.Invoke(bonusMult); });
        stateMan.onStateSwitch_ToFrom += OnSwitch;
        //Debug.Log(CurrentState);
    }
    int ap_state = Animator.StringToHash("state");
    int ap_gasRatio = Animator.StringToHash("gasRatio");
    int ap_chokeRatio = Animator.StringToHash("chokeRatio");
    int ap_valveOpen = Animator.StringToHash("isValveOpen");

    GasController gasCon;
    internal  bool crossedFinishLine;
    bool landComplete;
    int bonusMult;
    private void Start()
    {
        bonusMult = 1;
        crossedFinishLine = false;
        landComplete = false;
        gasCon = GetComponent<GasController>();
        gasCon.onGasChanged_current_total += OnGasChanged;
        gasCon.onValveStateChanged += OnValveChanged;
        GameplayManager.Instance.onPrepStart += () => { stateMan.SwitchState(GuyState.PUMP); };
        GameplayManager.Instance.onGameStart += () => { stateMan.SwitchState(GuyState.FLY); };
        punchAnim.transform.parent = null;
    }
    private void OnDestroy()
    {
        stateMan.onStateSwitch_ToFrom -= OnSwitch;
    }
    void OnSwitch(GuyState from, GuyState to)
    {
        anim.SetInteger(ap_state,(int) to);
    }
    void OnGasChanged(float current, float total)
    {
        float ratio = current / total;
        anim.SetFloat(ap_gasRatio, ratio);
        anim.SetFloat(ap_chokeRatio,1- ratio);

        if (ratio < 0.05f && CurrentState == GuyState.FLY) stateMan.SwitchState(GuyState.LAND);
    }
    void OnValveChanged(bool isOpen)
    {
        anim.SetBool(ap_valveOpen,isOpen);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "FinishLine" && !crossedFinishLine)
        {
            crossedFinishLine = true;
            onFinishLineCross?.Invoke(this);
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        int layerCheck = finishLayerMask & 1<<collision.collider.gameObject.layer;
        //if (log) Debug.LogFormat("{0}, {1}, {2}",gasCon.GasRatio, CurrentState, layerCheck);
        if (gasCon.GasRatio < 0.002f && CurrentState == GuyState.LAND && !landComplete && layerCheck!=0 )
        {
            landComplete = true;

            if (collision.collider.CompareTag("Bonus"))
            {
                bonusMult = collision.collider.GetComponentInParent<BonusID>().id;
                stateMan.SwitchState(GuyState.FINISH);
            }
            else
            {

                stateMan.SwitchState(GuyState.FAIL);
            }
        }
    }
}
public enum GuyState
{
    INIT= 0,
    PUMP = 1,
    FLY = 2,
    LAND = 3,
    FAIL = 4,
    FINISH = 5,
}
