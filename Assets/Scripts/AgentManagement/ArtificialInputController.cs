﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class ArtificialInputController : MonoBehaviour
{
    AgentStateController stateController;
    GasController playerGasController;
    [Header("AI params")]
    public float thrustDuration=1;
    public float thrurstDurationVariance=.25f;
    public float thrustGap=2;
    public float thrustGapVariance=.25f;
    public float angleToleranceInDegrees=20;

    public float injectGap = 0.2f;
    public Vector3 targetVector;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        playerGasController = GetComponent<GasController>();
        stateController = GetComponent<AgentStateController>();

        while (true)
        {
            switch (stateController.CurrentState)
            {
                case GuyState.PUMP:
                    {
                        playerGasController.InjectNow();
                        yield return new WaitForSeconds(injectGap);
                    }
                    break;
                case GuyState.LAND:
                case GuyState.FLY:
                    {
                        if (playerGasController.AngleWithinLimit(angleToleranceInDegrees))
                        {
                            playerGasController.SetValve(isOpen: true);
                            yield return new WaitForSeconds(Handy.Deviate(thrustDuration, thrurstDurationVariance));
                            playerGasController.SetValve(isOpen: false);
                            yield return new WaitForSeconds(Handy.Deviate(thrustGap, thrustGapVariance));
                        }
                        yield return null;
                    }
                    break;
                default:
                    yield return null;
                    break;
            }

        }
    }

}
