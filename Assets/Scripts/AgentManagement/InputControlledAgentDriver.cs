﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputControlledAgentDriver : MonoBehaviour
{
    AgentStateController stateController;
    GasController playerGasController;

    private void Start()
    {
        stateController = GetComponent<AgentStateController>();
        playerGasController = GetComponent<GasController>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (stateController.CurrentState)
        {
            case GuyState.PUMP:
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        playerGasController.InjectNow();
                    }
                }
                break;
            case GuyState.FLY:
            case GuyState.LAND:
                {
                    if (Input.GetMouseButton(0))
                    {
                        playerGasController.SetValve(isOpen: true);
                    }
                    else
                    {
                        playerGasController.SetValve(isOpen: false);
                    }
                }
                break;
            default:
                break;
        }
    }
}
