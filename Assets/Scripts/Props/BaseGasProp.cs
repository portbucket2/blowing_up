﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseGasProp : MonoBehaviour
{
     public Animator animator;
    public float gasChange = 10;
    public float chargeTime = 0.1f;
    public bool alive = true;
    public float GasForceRatio = 1;
    public virtual void Pop(GasController gc)
    {

    }
}
