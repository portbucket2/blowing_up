﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPropController : BaseGasProp
{

    public override void Pop(GasController gc)
    {
        if(gc.GetComponent<InputControlledAgentDriver>())
        {
            alive = false;
            animator.Play("pop");
            FRIA.Centralizer.Add_DelayedMonoAct(this,()=>{Destroy(this.gameObject);},0.5f);
            GameplayManager.Instance.AddCoinEaned();
        }
    }
}
