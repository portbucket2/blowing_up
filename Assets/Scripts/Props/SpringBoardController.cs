﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpringBoardController : MonoBehaviour
{
    public bool triggered;
    public Animator animator;
    public Transform forceDirection;
    public float forceValue= 30000;


    // Update is called once per frame
    public void Trigger()
    {
        triggered = true;
        FRIA.Centralizer.Add_DelayedMonoAct(this, ()=>
        {
            triggered = false;
        }, 1.25f);
        animator.SetTrigger("trig");
    }
}
