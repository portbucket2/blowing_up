﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeFishController : BaseGasProp
{

    public override void Pop(GasController gc)
    {

        animator.SetTrigger("trig");
        Vector3 dir = (gc.transform.position-this.transform.position).normalized;
        gc.StartCoroutine(gc.GasChange(gasChange,chargeTime,dir,GasForceRatio));
    }
    
}
