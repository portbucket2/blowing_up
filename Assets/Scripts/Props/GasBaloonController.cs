﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasBaloonController : BaseGasProp
{  
    public Collider sphCollider;

    public override void Pop(GasController gc)
    {
        alive = false;
        animator.Play("pop");
        FRIA.Centralizer.Add_DelayedMonoAct(this,()=>{Destroy(this.gameObject);},0.5f);
        Vector3 dir = (gc.transform.position-this.transform.position).normalized;
        gc.StartCoroutine(gc.GasChange(gasChange,chargeTime,dir,GasForceRatio));

    }
    

}
