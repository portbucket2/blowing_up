﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using FRIA;
public class GameplayManager : MonoBehaviour
{
    public static GameplayManager Instance { get; private set; }
    public List<AgentStateController> guys = new List<AgentStateController>();
    // Start is called before the first frame update

    public CurrencyTracker tracker;
    
    public Vector3 raceDirection = Vector3.right;
    public int countDownTime=3;
    public TMP_Text countDownText;
    //public GameObject instruction1;
    //public GameObject instruction2;
    StateSwitchController<GameState> gameStateMan;
    public event System.Action onPrepStart;
    public event System.Action onGameStart;
    public event System.Action onGameExit;
    public event System.Action onPlayerLost;
    public event System.Action<int> onPlayerWon_Bonus;

    bool firstTapDetected;
    private void Awake()
    {
        Instance = this;
        gameStateMan = new StateSwitchController<GameState>();
        gameStateMan.Init(true, true);
        gameStateMan.AddTransition(GameState.INIT, GameState.PREP, () => { onPrepStart?.Invoke(); });
        gameStateMan.AddTransition(GameState.PREP, GameState.GAME, () => 
        {
            onGameStart?.Invoke(); 
            CamSwapController.SetCamera(CamID.main);
        });
        gameStateMan.AddTransition(GameState.GAME, GameState.FAIL);
        gameStateMan.AddTransition(GameState.GAME, GameState.BONUS);
        gameStateMan.AddTransition(GameState.BONUS, GameState.SUCCESS);
        gameStateMan.AddStateExitCallback(GameState.GAME, ()=> { onGameExit?.Invoke(); });
        onPrepStart += () => { Debug.Log("prep a"); };
        onGameStart += () => { Debug.Log("game a"); };


        guys.AddRange(FindObjectsOfType<AgentStateController>());
        foreach (var item in guys)
        {
            if (item.GetComponent<InputControlledAgentDriver>())
            {
                playerController = item;
            }
            item.onFinishLineCross += AgentCrossedFinished;
        }
        playerController.onFail += PlayerFailed;
        playerController.onTouchDown_xBonus += PlayerTouchDown;
    }

    public AgentStateController playerController { get; private set; }
    IEnumerator Start()
    {
        CamSwapController.SetCamera(CamID.preinitial);
        agentsFinishedBeforePlayer = 0;
        countDownText.gameObject.SetActive(false);


        while (!firstTapDetected)
        {
            yield return null;
            if (Input.GetMouseButtonDown(0))
            {
                firstTapDetected = true;
            }
        }
        Debug.Log("call prep");
        gameStateMan.SwitchState(GameState.PREP);

        int textVal = countDownTime;
        countDownText.gameObject.SetActive(true);
        countDownText.text = textVal.ToString();
        for (int i = 0; i < countDownTime; i++)
        {
            yield return new WaitForSeconds(0.5f);
            textVal--;
            countDownText.text = textVal.ToString();
            yield return new WaitForSeconds(0.5f);
        }
        countDownText.gameObject.SetActive(false);
        Debug.Log("call game");
        gameStateMan.SwitchState(GameState.GAME);
        //instruction1.SetActive(false);
        //instruction2.SetActive(true);
        AnalyticsAssistant.LevelStarted(LevelManager.CurrentLevelNo);

    }
    int agentsFinishedBeforePlayer;
    bool countLocked;
    void AgentCrossedFinished(AgentStateController controller)
    {
        if (controller != playerController && !countLocked)
        {
            agentsFinishedBeforePlayer++;
        }
        else
        {
            countLocked = true;
        }
    }
    void PlayerFailed(AgentStateController controller)
    {
        TriggerLose();
    }
    void PlayerTouchDown(int bonus)
    {
        CamSwapController.SetCamera(CamID.celebration);
        if (agentsFinishedBeforePlayer > 0)
        {
            TriggerLose();
        }
        else
        {
            TriggerWin(bonus);
        }
    }
    void TriggerLose()
    {
        AnalyticsAssistant.LevelFailed(LevelManager.CurrentLevelNo);
        onPlayerLost?.Invoke();
    }
    void TriggerWin(int bonus)
    {
        AnalyticsAssistant.LevelCompleted(LevelManager.CurrentLevelNo);
        onPlayerWon_Bonus?.Invoke(bonus);
    }

    public int coinsEarned;
    public void AddCoinEaned()
    {
        coinsEarned++;
        Currency.Transaction(CurrencyType.CASH,1);
    }

}
public enum GameState
{
    INIT,
    PREP,
    GAME,
    FAIL,
    BONUS,
    SUCCESS,
}