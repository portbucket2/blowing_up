﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusIDDistributor : MonoBehaviour
{
    public MeshRenderer wall;
    public float scaleX = 16;
    public List<BonusID> items;
    public List<Material> materials;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < items.Count; i++)
        {
            items[i].Load(i+1,scaleX,materials[i]);
        }
        wall.material = materials[materials.Count-1];
        wall.transform.localPosition = new Vector3(scaleX*items.Count,wall.transform.localPosition.y,wall.transform.localPosition.z);
    }
}
