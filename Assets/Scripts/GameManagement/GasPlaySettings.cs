﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GasPlaySettings", menuName = "Gas Play Settings", order = 1)]
public class GasPlaySettings : ScriptableObject
{


    [Header("Misc")]
    public float initialBoostImpulseForce = 1500;
    [Header("Discharge")]
    public float gasLossRate = 20;
    public float gasToForceRatio = 200;
    public float drag = 2;
    public ForceMode forceMode = ForceMode.Force;
    public float noGasMass = 50;
    public float gasToMassRatio = 1;
    [Header("Gravity")]
    public AnimationCurve gravityByFill;
    public float maxG = 500;
    [Header("Swing")]
    public float frequency = 0.5f;
    public float maxSwing = 45;

    [Header("inject")]
    public int injectPerTap_min = 5;
    public int injectPerTap_max = 8;
    public float injectTime = 0.5f;
    public float gasLossPerSecondOnPump = 10;

    public float MassFromGas(float gasLeft)
    {
        return noGasMass + gasLeft * gasToMassRatio;
    }
    public Vector3 GetGravity(float gasRatio)
    {
        return -maxG * gravityByFill.Evaluate(1 - gasRatio) * Vector3.up;
    }
}
