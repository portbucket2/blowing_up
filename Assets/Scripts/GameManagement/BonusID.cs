﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BonusID : MonoBehaviour
{


    public Transform canvTrans;
    public TMP_Text text;
    public Transform block;
    public MeshRenderer mr;

    public int id;
    public void Load(int x, float xScale, Material m)
    {
        text.text = string.Format("{0}x",x);
        canvTrans.localPosition = canvTrans.localPosition + new Vector3 (0,4*Mathf.Sin(x*90*Mathf.PI/180),0);
        block.localScale = new Vector3(xScale,block.localScale.y,block.localScale.z);
        this.transform.localPosition = new Vector3(xScale*(x-1),0,0);
        mr.material = m;

        id = x;
    }

}
