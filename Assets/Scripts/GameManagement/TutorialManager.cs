﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class TutorialManager : MonoBehaviour
{
    public static HardData<bool> pumpTuorialComplete;
    public static HardData<bool> dischargeTutorialComplete;
    public Animator anim;
    public float angleLimit= 40;
    public GasController playerGasRef;

    void Start()
    {
        anim.gameObject.SetActive(false);
        if(pumpTuorialComplete == null) pumpTuorialComplete = new HardData<bool>("PUMP_TUT_COMPLETE",false);
        if(dischargeTutorialComplete == null) dischargeTutorialComplete = new HardData<bool>("DISCHARGE_TUT_COMPLETE", false);
        playerGasRef = GameplayManager.Instance.playerController.GetComponent<GasController>();
        GameplayManager.Instance.onPrepStart += () => 
        {
            StartCoroutine(RunPumpTut());
        };
        GameplayManager.Instance.onGameStart += () => 
        {
            pumpAwaiting = false;
            if (playerGasRef.GasRatio > 0.75f)
            {
                pumpTuorialComplete.value = true;
            }
            StartCoroutine(DisChargeTut());
        };
        GameplayManager.Instance.onGameExit += () =>
        {
            dischargeAwaiting = false;
        };
        GameplayManager.Instance.onPlayerWon_Bonus += (int bonus) =>
        {
            dischargeTutorialComplete.value = true;
        };
    }
    bool pumpAwaiting;
    bool dischargeAwaiting;
    IEnumerator RunPumpTut()
    {
        if (!pumpTuorialComplete.value)
        {
            //Debug.Log(playerGasRef.GasRatio);
            pumpAwaiting = true;
            anim.gameObject.SetActive(true);
            anim.SetTrigger("fastTap");
            while (pumpAwaiting &&  playerGasRef.GasRatio<0.99f)
            {
                yield return null;
            }
            anim.gameObject.SetActive(false);
        }
    }

    float cdTill;
    IEnumerator DisChargeTut()
    {

        yield return null;
        yield return null;
        if (!dischargeTutorialComplete.value)
        {
            dischargeAwaiting = true;
            anim.gameObject.SetActive(true);
            anim.SetTrigger("tapAndHold");
            yield return null;
            while (dischargeAwaiting && GameplayManager.Instance.playerController.CurrentState == GuyState.FLY)
            {
                if (!Input.GetMouseButton(0))
                {

                    if(Time.time>cdTill)anim.gameObject.SetActive(true);
                    //anim.SetTrigger("tapAndHold");
                }
                else
                {
                    cdTill = Time.time + 2;
                    anim.gameObject.SetActive(false);
                }
                yield return null;
            }
        }
    }

    void Update()
    {
        
    }
}
