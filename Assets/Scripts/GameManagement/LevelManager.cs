﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance { get; private set; }
    public TextAsset dataCSV;
    public List<LevelData> levelDataList = new List<LevelData>();
    public int defaultStartIndex=0;
    static HardData<int> currentLevelIndex;
    public static int CurrentLevelNo
    {
        get
        {
            return currentLevelIndex.value + 1;
        }
    }
    static HardData<int> lastUnlockedIndex;
    IEnumerator Start()
    {
        Instance = this;
        if(currentLevelIndex==null) currentLevelIndex = new HardData<int>("CURRENT_LEVEL_INDEX", defaultStartIndex);
        if(lastUnlockedIndex==null)lastUnlockedIndex = new HardData<int>("LAST_UNLOCKED_INDEX", defaultStartIndex);
        DontDestroyOnLoad(this.gameObject);
        CSVReader.Parse_toClassWithBasicConstructor<LevelData>(levelDataList, dataCSV);
        yield return null;

        LoadCurrent();
    }
    public static void UnlockNext()
    {
        if (lastUnlockedIndex.value == currentLevelIndex.value)
        {
            lastUnlockedIndex.value++;
        }
    }
    public static void LoadCurrent()
    {
        Instance.LoadIndex(currentLevelIndex);
    }
    public static void LoadNext()
    {
        currentLevelIndex.value++;
        Instance.LoadIndex(currentLevelIndex);
    }
    private void LoadIndex(int i)
    {
        int N = levelDataList.Count;
        int index = i % N;
        
        StartCoroutine(LoadRoutine(levelDataList[index]));
    }
    IEnumerator LoadRoutine(LevelData ld)
    {
        AsyncOperation asop = SceneManager.LoadSceneAsync(ld.sceneName);
        yield return asop;
        GameObject levelPlayable = Resources.Load<GameObject>(ld.designAsset);
        Instantiate(levelPlayable);
        Debug.Log("Loaded");
    }

}
[System.Serializable]
public class LevelData
{
    public int level_id;
    public int design_id;
    public int environment_id;

    public string designAsset
    {
        get
        {
            return string.Format("Levels/Level{0}", design_id);
        }
    }
    public string sceneName
    {
        get
        {
            return string.Format("Scene{0}", environment_id);
        }
    }
}