﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
public class EndCanvasManager : MonoBehaviour
{
    public GameObject failCanv;
    public GameObject succCanv;


    [Header("Fail")]
    public Button restartFail;
    [Header("Success")]
    public Button nextSucc;
    public TMP_Text coinTXT;
    public TMP_Text multTXT;
    public TMP_Text totalTXT;
    // Start is called before the first frame update
    void Start()
    {
        restartFail.onClick.AddListener(OnRestart);
        nextSucc.onClick.AddListener(OnNext);
        failCanv.SetActive(false);
        succCanv.SetActive(false);

        GameplayManager.Instance.onPlayerLost += LoadFail;
        GameplayManager.Instance.onPlayerWon_Bonus += LoadSucc;
    }

    void OnRestart()
    {
        if (LevelManager.Instance)
        {
            LevelManager.LoadCurrent();
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

    }
    void OnNext()
    {
        if (LevelManager.Instance)
        {
            LevelManager.LoadNext();
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            Debug.Log("Same scene loaded since levelmanager not found");
        }
    }

    void LoadFail()
    {
        failCanv.SetActive(true);
    }
    void LoadSucc(int bonus)
    {
        if (LevelManager.Instance)
        {
            LevelManager.UnlockNext();
        }
        int coins = GameplayManager.Instance.coinsEarned;
        coinTXT.text = coins.ToString();
        multTXT.text = string.Format("x{0}",bonus);
        int total = bonus*coins;
        int extra  = total-coins;
        totalTXT.text =  total.ToString();
        Currency.Transaction(CurrencyType.CASH,extra);
        succCanv.SetActive(true);
    }
}
